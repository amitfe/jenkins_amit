import sys
import os


def sqr(num):
    for i in range(int(float(num))):
        print(i, i**2)


def main():
    try:
        num = os.environ['num']
    except KeyError:
        if len(sys.argv) > 1:
            num = sys.argv[1]
        else:
            print("Please provide number to square")
            sys.exit(1)
    sqr(num)


if __name__ == '__main__':
    main()
