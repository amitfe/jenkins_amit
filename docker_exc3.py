import requests
from bs4 import BeautifulSoup
import os
import sys

HEADERS = {'User-Agent': 'Mozilla/5.0'}


def links_to_html(links):
    for link in links:
        try:
            url = requests.get(link, headers=HEADERS)
        except requests.exceptions.MissingSchema:
            print(f"Invalid URL {link}")
            continue
        except Exception as error:
            print(f'Error parsing {link}')
        if url.status_code == requests.codes.ok:
            soup = BeautifulSoup(url.content, 'html.parser')
            print(soup.prettify())
        else:
            print(f"Link {link} could no be reached")
            continue


def main():
    try:
        links = os.environ['links']
    except KeyError:
        if len(sys.argv) > 1:
            links = sys.argv[1]
        else:
            print("No input, please provide a list of links in a square brackets")
            sys.exit(1)
    links = links.split(',')
    links_to_html(links)


if __name__ == '__main__':
    main()